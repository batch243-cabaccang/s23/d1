function Menu(name, price) {
  this.menuName = name;
  this.menuPrice = price;
}

const mealSetOne = new Menu("Kid's Meal", "Free");
const mealSetTwo = new Menu("Adult's Meal", "Not Free");
const mealArray = [mealSetOne, mealSetTwo];

console.log(mealArray[1].menuName);
